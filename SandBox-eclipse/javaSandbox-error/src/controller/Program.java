package controller;

public class Program {

	public static void main(String[] args) {
		
		int[] simpleArray = {1,2,3,4,5,6,7,8,9};
		
		try {
		
			for (int index = 0; index <= simpleArray.length; index++) {
				System.out.println("simpleArray[" + index + "] = " + simpleArray[index]);
			}
			
		} catch(ArrayIndexOutOfBoundsException exc) {
			System.err.println("Error! " + exc);
		}
		
	}

}
