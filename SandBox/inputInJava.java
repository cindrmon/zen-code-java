import java.io.*;

public class inputInJava {

    public static void main(String args[]) {

        try{

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Enter your name in java (note: must be written in java): ");
            
            String strName = reader.readLine();
            System.out.println("Hello " + strName + " in java!");

        } catch (Exception e){}

    }


}