public class CoutEmulator{


    public static void main(String[] args) {
        CoutEmulator s = new CoutEmulator();

        s.cout("word").cout(" can").cout(" be").cout(" chained");
        
    }
    private CoutEmulator cout(String msg, Object... params) {
        System.out.print(String.format(msg, params));
        return this;
    }


}