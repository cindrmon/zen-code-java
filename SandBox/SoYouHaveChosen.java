import java.io.*;

public class SoYouHaveChosen {

    public static void main(String[] args) {

        try {
            // for input
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            // actual variables
            String DeathChoice, Pass = "YES";
    
            // what is printed
            System.out.println("Do you wanna die?");
            DeathChoice = reader.readLine();

            // decision
            if(!DeathChoice.equalsIgnoreCase(Pass))
                System.out.println("So you have chosen death");
            else 
                System.out.println("I see... Well, you die anyways");
            
        } catch (Exception e) {
            System.err.println(e);
        }
        

    }

}