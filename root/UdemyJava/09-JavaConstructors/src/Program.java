
class Machine {
	
	private String name;
	private int code;
	
	public Machine() {
		// this is a constructor
		this("null", 0); // needs to be the first line
		name = "null";
		System.out.println("This is a constructor running.");
		System.out.println("Default name used.");
	}
	public Machine(String name) {//this is stright up polymorphism
		this(name, 0); // needs to be the first line
		this.name = name;
		System.out.println("This is another constructor running.");
		System.out.println("Name is now " + this.name);
	}
	public Machine(String name, int code) {
		this.name = name;
		this.code = code;
		System.out.println("This is a third constructor running.");
		System.out.println("Name " + this.name + " has now a code " + this.code);
	}
	
}

public class Program {

	public static void main(String[] args) {
		
		Machine mach1 = new Machine();
		
		new Machine();
		new Machine("Smith");
		new Machine("John", 123456);

	}

}
