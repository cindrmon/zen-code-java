package controller;

import java.util.ArrayList;
import java.util.List;

public class ArrayListProgram {

	public static void main(String[] args) {
		
		ArrayList<String> words = new ArrayList<String>();
		
		// Inserting
		words.add("Hello!");
		words.add("Goodbye!");
		words.add("Welcome!");
		
		// Reading one
		System.out.println(words.get(2));
		
		// reading all using indexed for
		for (int idex = 0; idex < words.size(); idex++) {
			System.out.println(words.get(idex));	
		}
		
		// foreach method
		for(String item : words) {
			System.out.println(item);
		}
		
		 List<String> anotherWords = new ArrayList<String>();

	}

}
