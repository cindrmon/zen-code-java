package controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListProgram {

	public static void main(String[] args) {
		
		List<Integer> numberArrayList = new ArrayList<Integer>(); // add and remove items at the end of list
		List<Integer> numberLinkedList = new LinkedList<Integer>(); // add and remove items from anywhere in the list
		
		doTimings("ArrayList", numberArrayList);
		doTimings("LinkedList", numberLinkedList);
		
	}
	
	private static void doTimings(String type, List<Integer> list) {
		
		
		long start = System.currentTimeMillis();
		// add items at end of list
//		for(int idex=0 ; idex < 1E6; idex++) {
//			list.add(idex);
//		}
		
		// add items from anywhere on the list
		for(int idex=0; idex < 1e6; idex++) {
			list.add(0, idex);
		}
		long end = System.currentTimeMillis();
		
		System.out.println("Time Taken: " + (end - start) + " ms for " + type);
	}

}
