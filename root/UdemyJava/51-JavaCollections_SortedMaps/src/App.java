import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

class Temp {

}

public class App {
    public static void main(String[] args) {

        Map<Integer, String> sampleHashMap = new HashMap<Integer, String>();
        Map<Integer, String> sampleLinkedHashMap = new LinkedHashMap<Integer, String>();
        Map<Integer, String> sampleTreeMap = new TreeMap<Integer, String>();

        // testYourMap(sampleHashMap);
        // testYourMap(sampleLinkedHashMap);
        testYourMap(sampleTreeMap);

    }

    public static void testYourMap(Map<Integer, String> testMap) {
        testMap.put(4, "Fa");
        testMap.put(1, "Do");
        testMap.put(3, "Mi");
        testMap.put(6, "La");
        testMap.put(2, "Re");
        testMap.put(0, "Nvll");
        testMap.put(7, "Ti");
        testMap.put(5, "So");

        for (Integer key : testMap.keySet()) {
            String value = testMap.get(key);

            System.out.println(key + ": " + value);
        }
    }

}
