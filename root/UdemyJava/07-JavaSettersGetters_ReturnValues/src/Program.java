
class HumanBeing {
	// classes can contain
	// 1. Data	
	// 2. Subroutines (methods/functions)
	
	// Instance Variables (data/"state")
	private String HumanName;
	private int Age;
	
	// Setters/Getters
	
	// setters set/input the value into the object

	public void setName(String HumanName) {
		this.HumanName = HumanName;
	}
	
	public void setAge(int Age) {
		this.Age = Age;
	}
	
	// getters get/output the value out through the object
	
	public String getName() {
		return HumanName;
	}
	
	public int getAge() {
		return Age;
	}
	
	// Subroutines/Methods
	public void sayMyDetails() {
		System.out.println("My Name is " + HumanName + " and I'm here to say,");
		System.out.println("My age is " + Age + "...");
	
	}
	
	public void sayHello() {
		System.out.println("Hello there, fellow human!");
	}

	public int calculateYearsToRetirement() {
		
		if(Age >= 65)
			return 0;
		else
			return 65 - Age;
		
	}
	
}

public class Program {
	
	// subroutine example
	public static void main(String[] args) {
		HumanBeing Person1 = new HumanBeing();
		
		// without setter methods
		//Person1.HumanName = "James Gosling";
		//Person1.Age = 65;
		
		// with setter methods
		Person1.setName("James Gosling");
		Person1.setAge(65);
		
		HumanBeing Person2 = new HumanBeing();
		
		// without setter methods
		//Person2.HumanName = "Bjarne Stroustrup";
		//Person2.Age = 69;
		
		// with setter methods
		Person2.setName("Bjarne Stroustrup");
		Person2.setAge(69);
		
		HumanBeing Person3 = new HumanBeing();
		
		// without setter methods
		//Person3.HumanName = "John Smith";
		//Person3.Age = 25;
		
		// with setter methods
		Person3.setName("John Smith");
		Person3.setAge(25);
		
		
		String name;
		int age;
		
		name = Person1.getName();
		age = Person1.getAge();
		
		System.out.println("Person 1 Details: ");
		System.out.println("Name: " + name);
		System.out.println("Age: " + age);
		System.out.println("Years left till retirement: " + Person1.calculateYearsToRetirement());
		
		System.out.println();
		
		name = Person2.getName();
		age = Person2.getAge();
		
		System.out.println("Person 2 Details: ");
		System.out.println("Name: " + name);
		System.out.println("Age: " + age);
		System.out.println("Years left till retirement: " + Person2.calculateYearsToRetirement());
		
		System.out.println();
		
		name = Person3.getName();
		age = Person3.getAge();
		
		System.out.println("Person 3 Details: ");
		System.out.println("Name: " + name);
		System.out.println("Age: " + age);
		System.out.println("Years left till retirement: " + Person3.calculateYearsToRetirement());
			
		System.out.println();
		
		// When you let the person speak:
		Person1.sayMyDetails();
		Person2.sayMyDetails();
		Person3.sayMyDetails();
		
		Person1.sayHello();

	}

}
