
class RandomThing{
	public String aName; // decentralized variables; owned by object
	public static String aDescription; // centralized variables; owned by class
	public int id;
	
	public final static int LUCKY_NUMBER = 777; // final is const in java
	
	public static int NoOfObjects = 0;
	
	public RandomThing() {
		
		id = (int)(Math.random()*1000000000)+1;
		
		++NoOfObjects;
	}
	
	public void OutputName() {
		System.out.println(aName);
	}
	
	public void OutputID() {
		System.out.println(id);
	}
	
	public static void OutputInfo() {
		System.out.println(aDescription + " UwU");
	}
	
	public void OutputFormal() {
		System.out.println(aDescription + aName);
	}
}

public class Program {

	public static void main(String[] args) {
		
		RandomThing.aDescription = "this is ";
		
		System.out.println("Object count before making objects " + RandomThing.NoOfObjects);
		
		RandomThing aThing1 = new RandomThing();
		RandomThing aThing2 = new RandomThing();
		
		System.out.println("The description prefix set is: " + RandomThing.aDescription);
		
		aThing1.aName = "Don";
		aThing2.aName = "Suzy";
		
		System.out.print("aThing1.id = ");
		aThing1.OutputID();
		System.out.print("aThing1.aName = ");
		aThing1.OutputName();
		aThing1.OutputFormal();
		
		System.out.println();
		
		System.out.print("aThing2.id = ");
		aThing2.OutputID();
		System.out.print("aThing2.aName = ");
		aThing2.OutputName();
		aThing2.OutputFormal();
		
		RandomThing.OutputInfo();
		
		System.out.println(RandomThing.LUCKY_NUMBER);
		
		System.out.println("Object count after making objects " + RandomThing.NoOfObjects);
	}

}
