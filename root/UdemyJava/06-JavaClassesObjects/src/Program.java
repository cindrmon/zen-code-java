
class HumanBeing {
	// classes can contain
	// 1. Data	
	// 2. Subroutines (methods/functions)
	
	// Instance Variables (data/"state")
	String HumanName;
	int Age;
	
	// Subroutines/Methods
	void sayMyDetails() {
		System.out.println("My Name is " + HumanName + " and I'm here to say,");
		System.out.println("My age is " + Age + "...");
	
	}
	
	void sayHello() {
		System.out.println("Hello there, fellow human!");
	}

}

public class Program {
	
	// subroutine example
	public static void main(String[] args) {
		HumanBeing Person1 = new HumanBeing();
		
		Person1.HumanName = "James Gosling";
		Person1.Age = 65;
		
		HumanBeing Person2 = new HumanBeing();
		
		Person2.HumanName = "Bjarne Stroustrup";
		Person2.Age = 69;
		
		System.out.println("Person 1 Details: ");
		System.out.println("Name: " + Person1.HumanName);
		System.out.println("Age: " + Person1.Age);
		
		System.out.println();
		
		System.out.println("Person 2 Details: ");
		System.out.println("Name: " + Person2.HumanName);
		System.out.println("Age: " + Person2.Age);
		
		System.out.println();
		
		// When you let the person speak:
		Person1.sayMyDetails();
		Person2.sayMyDetails();
		
		Person1.sayHello();

	}

}
