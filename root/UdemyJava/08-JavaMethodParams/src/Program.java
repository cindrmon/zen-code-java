class Robot {
	public void Speak(String name) {
		System.out.println("\"Hello " + name + "\", says the robot");
	}
	public void Jump(int height) {
		System.out.println("The robot is jumping " + height + " metres.");
	}
	
	public void Move(String direction, double distance) {
		System.out.println("The robot is moving " + distance + " metres " + direction + ".");
	}
	
	public void Murder(String FullName) {
		System.out.println(FullName + " is now written in your digital death note. They will now die of heart attack in the next 20 seconds.");
	}
}


public class Program {

	public static void main(String[] args) {
		
		Robot aBot = new Robot();
		String aName = "Sam";
		String FullName = "Ecru Adams";
		
		
		aBot.Speak(aName);
		aBot.Jump(14);
		aBot.Move("west", 3.14159265);
		aBot.Murder(FullName);

	}

}
