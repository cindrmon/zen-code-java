
public class ProgramMultArrays {

	public static void main(String[] args) {
		//Multi-Dimensional Assay Example:
		
		int[][] matrix = {
			{1,2,3},
			{4,5,6},
			{7,8,9}
		};
		
		// there's a difference between println and print
		// println prints out a line then automatically "\n"s
		// print is basically just like any other print, like the c++'s cout
		System.out.println();
			
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println();
		
		int ROWS = 2, COLS = 3;
		
		String[][] texts = new String[ROWS][COLS];
		
		texts[0][1] = "Hi there!";
		texts[1][1] = "howdy do!";
		
		for (int i = 0; i < ROWS; i++) {
			System.out.println("Row " + (i+1));
			for (int j = 0; j < COLS; j++) {
				System.out.println(texts[i][j]);
			}
		}
		
	}

}
