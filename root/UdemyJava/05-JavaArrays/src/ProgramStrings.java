
public class ProgramStrings {

	public static void main(String[] args) {

		String[] words = new String[5];
		
		words[0] = "aaaaaaa";
		words[1] = "bbbbbbb";
		words[2] = "ccccccc";
		words[3] = "ddddddd";
		words[4] = "eeeeeee";
		
		// for loop access
		for (int i = 0; i < words.length; i++) {
			System.out.println(words[i] + " ");
		}
		
		System.out.println();
		
		// for each loop access
		String[] names = {"Apple", "John", "Pear"};
		
		for (String name : names) {
			
			System.out.println(name + " ");
			
		}
		
		// lowercase is primitive, uppercase is classes
		
		int value = 10;
		String text = "a string of text";
		
		System.out.println("Value is " + value + ", and String is " + text + ".");
		
		
		
	}

}
