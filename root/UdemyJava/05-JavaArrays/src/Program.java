import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int value = 10;
		
		//Option A
		int[] values;
		
		values = new int[5];
		
		for (int i = 0; i < values.length; i++) {
			
			System.out.println("Enter value " + (i+1) + ": ");
			values[i] = in.nextInt();
			
		}

		System.out.println("Singular Value: " + value);
		
		for(int idx = 0; idx < values.length; ++idx) {
			System.out.println("Value " + (idx+1) + ": " + values[idx]);
		}
		
		// Option B
		int[] numberset = {1, 2, 3, 4, 6, 11};
		
		for (int i = 0; i < numberset.length; i++) {
			
			System.out.println("Values in numberset[" + i + "]: " + numberset[i]);
			
		}
		
		
	}

}
