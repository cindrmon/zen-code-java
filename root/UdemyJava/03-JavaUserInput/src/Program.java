// input package
import java.util.Scanner;

public class Program {
	
	public static void main(String[] args) {
		
		// create Scanner object
		@SuppressWarnings("resource")
		Scanner inputValue = new Scanner(System.in);

		// output prompt
		System.out.println("Enter String: ");
		// wait for the user to enter a line of text
		String inputLine = inputValue.nextLine();
		
		System.out.println("Now, enter a value (int)");
		int inputInteger = inputValue.nextInt();
		
		System.out.println("Now, enter a decimal value (double)");
		double inputDouble = inputValue.nextDouble();
		
		// output inputed line of text
		System.out.println("You entered: " + inputLine);
		System.out.println("Your integer is: " + inputInteger);
		System.out.println("Your double is: " + inputDouble);
		
	}

}
