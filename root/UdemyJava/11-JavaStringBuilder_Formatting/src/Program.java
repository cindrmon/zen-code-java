
public class Program {

	public static void main(String[] args) {
		
		// bad practice 
		/*
		String sampleString = "";
		
		sampleString += "Hello!";
		sampleString += " ";
		sampleString += "OwO What's this?";
		
		System.out.println(sampleString);
		*/
		
		// good practice, with StringBuilder
		StringBuilder sampleString = new StringBuilder("");

		sampleString.append("Hello!");
		sampleString.append(" ");
		sampleString.append("OwO What's this?\n");
	
		System.out.println(sampleString.toString());
		
		// StringBuilder cout emulator (for those who miss cout)
		StringBuilder cout = new StringBuilder();
		
		cout.append("OwO What's this? \n\n")
		.append("I can daisy chain multiple lines?")
		.append(" Interesting to know that I can do this UwU.");
		
		System.out.println(cout.toString());
		
		// String Formatting
		System.out.println("Default Text.\n");
		System.out.println("\\n is enter, \\t is tab.\n");
		System.out.println("\tThis is tabbed!\n\n");
		
		// C-like String Formatting
		System.out.printf("Sample Cost: %7d\nSample Quantity: %d", 100, 120);
		
		
		
		
		
		
		
		
		
		
		
	}

}
