
public class Program {

	public static void main(String[] args) {
		
		int SampleVar;
		double DecimalVar;
		float SmallDecimalVar;
		
		SampleVar = 5;
		
		System.out.println("Sample Var is: " + SampleVar);

		SampleVar = 512;
		
		System.out.println("New var is: " + SampleVar);
		
		DecimalVar = 5.123;
		
		System.out.println("Sample double is: " + DecimalVar);
		
		SmallDecimalVar = 5.1234f;
		
		System.out.println("Sample float is: " + SmallDecimalVar);
		
	}

}
