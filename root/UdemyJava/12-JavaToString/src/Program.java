
class Animal {

	private int id;
	private String name;
	
	public Animal(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	// inherited method for all objects
	// toString always blurts out an ostream of a particular object
	// or something
	public String toString() {
		StringBuilder ostream = new StringBuilder();
		
		ostream.append(id).append(": ").append(name);
		 
		return ostream.toString();
	}

	
}

public class Program {

	public static void main(String[] args) {
		
		Animal frog = new Animal(0001, "OwO");
		Animal cat = new Animal(0002, "What's This?");
		
		System.out.println(frog);
		System.out.println(cat);

	}

}
