package util;

import java.io.*;

public class EraserThread implements Runnable {
	private boolean stop = true;

	
	/** 
	 * @param The prompt displayed to the user
	 * */
	public EraserThread(String prompt) {
		System.out.println(prompt);
	}
	
	/** 
	 * Begin masking, using display asterisks (*)
	 * */
	public void run() {
		while(stop) {
			System.out.print("\010*");
			try {
				Thread.currentThread().sleep(1);
			} catch (InterruptedException iExc) {
				iExc.printStackTrace();
			}
		}
	}
	
	/**
	 * Instruct the thread to stop masking
	 * */
	public void stopMasking() {
		this.stop = false;
	}
}
