package util;

import java.io.*;
public class PasswordField {

	/** 
	 * @param prompt The prompt to display the user
	 * @return The password as entered by the user
	 * */
	public static String readPassword(String prompt) {
		EraserThread eth = new EraserThread(prompt);
		Thread mask = new Thread(eth);
		mask.start();
		
		BufferedReader inStream = new BufferedReader(new InputStreamReader(System.in));
		String password = "";
		
		try {
			password = inStream.readLine();
		} catch (IOException ioExc) {
			ioExc.printStackTrace();
		}
		
		// stop masking
		eth.stopMasking();
		// return the password enterd by the user
		return password;
		
	}
	
}
