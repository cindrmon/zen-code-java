package model;

import exception.InvalidNumberOfGearsException;
import java.io.Serializable;

public class BicycleBean implements Serializable{
	
	//attributes or instance variables
	private int gear;
	private String brand;
	private String type;
	private String model;
	private double price;
	private String url;
	
	//////////////////////
	//setter (mutator) and getter (accessor) methods
	public int getGear() {
		return gear;
	}

	/** compile-time custom exception method A (trabahong tamad)
	 * 
	 * 	throws and passes the responsibility 
	 * to someone else
	 *  */
	
//	public void setGear(int gear) throws InvalidNumberOfGearsException { 
//		// opt too handle trigger of GearException
//		if(gear > 36) {
//			// throws exception
//			throw new InvalidNumberOfGearsException();
//		} else {
//			this.gear = gear;
//		}	
//	}
	
	/** compile-time custom exception method B (best practice)
	 * 
	 * 	tries and catches a thrown exception, 
	 * meaning it will have the responsibility 
	 * to catch thrown exception
	 *  */
	public void setGear(int gear) { 
		// trycatch handle exception within itself
		try {
			if(gear > 36) {
				// throws exception
				throw new InvalidNumberOfGearsException();
			} else {
				this.gear = gear;
			}
		} catch(InvalidNumberOfGearsException exc) {
			System.err.println(exc.getMessage());
		}
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	//instance methods or functions
	public void accelerate() {
		System.out.println(type + " accelerates.");
	}
	
	public void braking() {
		System.out.println(type + " brakes.");
	}
}
