package exception;

import exception.message.ExceptionMessage;

// compile-time exception (checked)
public class InvalidNumberOfGearsException extends Exception {
	// define constructors
	public InvalidNumberOfGearsException() {
		super(ExceptionMessage.INVALID_NUM_OF_GEARS);
	}
	public InvalidNumberOfGearsException(String message) {
		super(message);
	}
}
