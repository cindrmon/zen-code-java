package controller;

import view.Display;

import util.KBInput;

import model.bank.*;
import model.regulator.*;

public class BanksController {
	
	public static void main(String[] args) {
		
		String choice;
		BSP aBank;

		/**
		 * BSP is the main abstract class, and BPI, Metrobank, and SecurityBank are its
		 * children.
		 * 
		 * Class widening is done like this: 
		 * 
		 * BSP bank = new BPI();
		 * Display.print(bank);
		 */
		
		choice = KBInput.readString("Enter Bank: ");

		if(choice.equalsIgnoreCase("BPI")) {
			aBank = new BPI();
			Display.print(aBank);
		}
		else if(choice.equalsIgnoreCase("Metrobank")) {
			aBank = new Metrobank();
			Display.print(aBank);
		}
		else if(choice.equalsIgnoreCase("Security Bank")) {
			aBank = new SecurityBank();
			Display.print(aBank);
		}
		
	}

}
