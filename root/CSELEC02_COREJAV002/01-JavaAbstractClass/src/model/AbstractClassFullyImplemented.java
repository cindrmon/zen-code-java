package model;

public abstract class AbstractClassFullyImplemented {
	
	public AbstractClassFullyImplemented() {
		
	}
	
	public AbstractClassFullyImplemented(String message) {
		
	}
	
	public void message() {
		// fully implemented if there are braces
		
		System.out.println("Hello from a method in an abstract class");
	}
	
	public void message(String message) {
		System.out.println(message);
	}
	
	abstract public void greet();
}
