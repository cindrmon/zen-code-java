package example.demo;

import java.io.*;

public class ExceptionUnCheckedDemo {

	public static void main(String[] args) {
		int iNum1 = 6;
		int iNum01 = 1;
		
		try {
			System.out.println("Quotient is " + (iNum1/iNum01));
		} catch(ArithmeticException arithExc) {
			System.err.println("Hey!");
			System.err.println("Exception is: " + arithExc.getMessage());
		
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
				System.out.print("Enter a number: ");
				String stringOut = reader.readLine();
				
				int iNum2 = Integer.parseInt(stringOut);
				
			} catch(IOException ioExc) {
				System.err.println("Caught IO Exception: " + ioExc.getMessage());
			} catch(NumberFormatException numFmtExc) {
				System.err.println("Not convertible to a double data type.");
				System.err.println("Caught Number Format Exception: " + numFmtExc.getMessage());
			} catch(Exception exc) {
				System.err.println("Caught General Exception: " + exc.getMessage());
			}
			
			System.out.println("INSIDE CATCH STATEMENT");
			
		} catch(Exception exc) {
			System.err.println("Caught General Exception: " + exc.getMessage());
		} finally {
			System.out.println("All Dne!");
		}
		
		System.out.println("\n\nProgram Finished.");

	}

}
