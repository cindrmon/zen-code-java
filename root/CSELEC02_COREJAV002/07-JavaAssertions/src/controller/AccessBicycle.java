package controller;

import model.BicycleBean;
import view.Display; 
import exception.message.*;

public class AccessBicycle {
	public static void main(String[] args) {

		int numberOfGears = 40;
		
		assert(numberOfGears <= 36): "Value is greater than 36"; 
		// if assert is true => nothing happens
		// else => AssertionError
		
		BicycleBean pinarelloDogma = new BicycleBean();
		pinarelloDogma.setGear(numberOfGears);
		pinarelloDogma.setModel("Pinarello Dogma F12 Champagnolo");
		pinarelloDogma.setBrand("Pinarello");
		pinarelloDogma.setType("Touring");
		pinarelloDogma.setPrice(700000);
		pinarelloDogma.setUrl("https://upgrade.ph/shop/brands/pinarello" 
			+ "/pinarello-dogma-f12-disk-built-bike-campagnolo-super-record-eps/");
		
		BicycleBean cerveloP5 = new BicycleBean(); //let us create our second bike
		cerveloP5.setGear(24);
		cerveloP5.setModel("Cervelo P5");
		cerveloP5.setBrand("Cervelo");
		cerveloP5.setType("Touring");
		cerveloP5.setPrice(765000);
		cerveloP5.setUrl("https://upgrade.ph/shop/bike/cervelo/cervelo-p5-"
			+ "disc-2019-tri-tt-bike-dura-ace-9180-di2/");
		
		BicycleBean someBikeOffTheStreet = new BicycleBean();
		someBikeOffTheStreet.setGear(4);
		someBikeOffTheStreet.setModel("Generic Model");
		someBikeOffTheStreet.setBrand("Generic Brand");
		someBikeOffTheStreet.setType("Road");
		someBikeOffTheStreet.setPrice(0);
		someBikeOffTheStreet.setUrl("https://sfbay.craigslist.org/d/bicycles-by-owner/search/bik");
		
		Display.print(pinarelloDogma);	
		Display.print(cerveloP5);
		Display.print(someBikeOffTheStreet);
		
		
	}
}
