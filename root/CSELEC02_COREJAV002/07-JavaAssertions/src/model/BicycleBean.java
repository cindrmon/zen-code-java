package model;

import exception.InvalidNumberOfGearsException;
import java.io.Serializable;

public class BicycleBean implements Serializable{
	
	//attributes or instance variables
	private int gear;
	private String brand;
	private String type;
	private String model;
	private double price;
	private String url;
	
	//////////////////////
	//setter (mutator) and getter (accessor) methods
	public int getGear() {
		return gear;
	}
	
	// no trycatch in runtime exception
//	public void setGear(int gear) {
//		if(gear > 36) {
//			throw new InvalidNumberOfGearsException();
//		} else {
//			this.gear = gear;
//		}
//	}
	
	// trycatch in runtime excecption
	public void setGear(int gear) {
		try {
		if(gear > 36) {
			throw new InvalidNumberOfGearsException();
		} else {
			this.gear = gear;
		}} catch(InvalidNumberOfGearsException exc) {
			System.err.println(exc.getMessage());
		}
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	//instance methods or functions
	public void accelerate() {
		System.out.println(type + " accelerates.");
	}
	
	public void braking() {
		System.out.println(type + " brakes.");
	}
}
