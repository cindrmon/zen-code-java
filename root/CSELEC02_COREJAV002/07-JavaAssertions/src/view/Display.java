package view;

import model.BicycleBean;

public class Display {

	public static void print(BicycleBean bike) {
		System.out.println("\nModel: " + bike.getModel());
		System.out.println("Manufacturer/Brand: " + bike.getBrand());
		System.out.println("Type: " + bike.getType());
		System.out.println("Total Gears: " + bike.getGear());
		System.out.println("Price: Php" + bike.getPrice());
		System.out.println("URL: " + bike.getUrl() + "\n");
		//now call the methods of the Bicycle class
		bike.accelerate();
		bike.braking();
	}
}

