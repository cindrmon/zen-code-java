package view;

import model.regulator.*;
import model.bank.*;

public class Display {
	
	public static void print(BSP bank) {
		
		if (bank instanceof BPI) {
			BPI bpi = (BPI) bank;
			bpi.assignCustodianship("sample custodian");
			bpi.assignExternalAuditor("sample auditor");
			bpi.buySharesFromUSCompanies();
			bpi.investmentNotInsuredAnnouncement();
			bpi.taxAnnouncement();
		}
		else if(bank instanceof Metrobank) {
			Metrobank metrobank = (Metrobank) bank;
			metrobank.assignCustodianship("sample custodian 2");
			metrobank.assignExternalAuditor("sample auditor 2");
			metrobank.buySharesFromUSCompanies();
			metrobank.investmentNotInsuredAnnouncement();
			metrobank.taxAnnouncement();
		}
		else if(bank instanceof SecurityBank) {
			SecurityBank securityBank = (SecurityBank) bank;
			securityBank.assignCustodianship("sample custodian 3");
			securityBank.assignExternalAuditor("sample auditor 3");
			securityBank.buySharesFromUSCompanies();
			securityBank.investmentNotInsuredAnnouncement();
			securityBank.taxAnnouncement();
		}
		
	}
	
}
