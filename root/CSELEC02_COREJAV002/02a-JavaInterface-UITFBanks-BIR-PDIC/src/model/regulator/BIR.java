package model.regulator;

public interface BIR {
	/**
	 * Variables declared in an interface are always
	 * `public static final`.
	 * 
	 * All fully unimplemented methods in an interface 
	 * are always `public abstract`.
	 *  */
	String TAX_PERCENTAGE = "20%";
	void taxAnnouncement();

}
