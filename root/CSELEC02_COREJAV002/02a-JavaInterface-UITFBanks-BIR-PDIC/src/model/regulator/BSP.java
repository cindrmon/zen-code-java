package model.regulator;

public abstract class BSP {

	public BSP() {
		System.out.print("ALL UITF investments are managed and regulated by");
		System.out.print(" The Bangko Sentral ng Pilipinas (BSP)\n\n");
	}
	
	public abstract void assignCustodianship(String custodianship);
	public abstract void assignExternalAuditor(String auditor);
//	public abstract void buySharesFromUSCompanies(Object[] shares);
	public abstract void buySharesFromUSCompanies();
	
	
}
