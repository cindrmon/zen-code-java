package view;

import java.sql.ResultSet;
import java.sql.SQLException;
import util.Security;
import model.BicycleBean;

public class Display {

	public static void print(BicycleBean bike) {
		System.out.println("\nModel: " + bike.getModel());
		System.out.println("Manufacturer/Brand: " + bike.getBrand());
		System.out.println("Type: " + bike.getType());
		System.out.println("Total Gears: " + bike.getGear());
		System.out.println("Price: Php" + bike.getPrice());
		System.out.println("URL: " + bike.getUrl() + "\n");
		//now call the methods of the Bicycle class
		bike.accelerate();
		bike.braking();
	}
	
	public static void printAllRecords(ResultSet records) {
		System.out.println("\nDisplaying Records...\n\n");
		if(records != null) {
			System.out.println("ID\t\t\tBrand\tModel\t\t\tPrice\tType\tGear\tURL");
			
			try {
				while(records.next()) {
					System.out.print(records.getInt("bikeID") + "\t");
					System.out.print(Security.decrypt(records.getString("brand")) + "\t");
					System.out.print(Security.decrypt(records.getString("model")) + "\t");
					System.out.print(records.getDouble("price") + "\t");
					System.out.print(Security.decrypt(records.getString("type")) + "\t");
					System.out.print(records.getString("gear") + "\t");
					System.out.println(Security.decrypt(records.getString("url")));
				}
			} catch (SQLException sqlExc) {
				System.err.println("Cannot connect to Database... " + sqlExc.getMessage());
			}
			
		} else {
			System.out.println("Records are Empty!");
		}
	}
}

