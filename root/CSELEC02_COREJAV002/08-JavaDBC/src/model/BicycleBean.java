package model;

import exception.InvalidNumberOfGearsException;
import java.io.Serializable;
import java.sql.*;
import util.DBOperation;

public class BicycleBean implements Serializable{
	
	//attributes or instance variables
	private int gear;
	private String brand;
	private String type;
	private String model;
	private double price;
	private String url;
	
	//////////////////////
	//setter (mutator) and getter (accessor) methods
	public int getGear() {
		return gear;
	}
	
	// no trycatch in runtime exception
//	public void setGear(int gear) {
//		if(gear > 36) {
//			throw new InvalidNumberOfGearsException();
//		} else {
//			this.gear = gear;
//		}
//	}
	
	// trycatch in runtime excecption
	public void setGear(int gear) {
		try {
		if(gear > 36) {
			throw new InvalidNumberOfGearsException();
		} else {
			this.gear = gear;
		}} catch(InvalidNumberOfGearsException exc) {
			System.err.println(exc.getMessage());
		}
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	//instance methods or functions
	public void accelerate() {
		System.out.println(type + " accelerates.");
	}
	
	public void braking() {
		System.out.println(type + " brakes.");
	}

	/*
	 * JDBC Operations
	 * */
	
	private Connection getConnection() {
		Connection connection = null;
		
		try {
			// perform JDBC driver enrollment
			Class.forName("com.mysql.jdbc.Driver");
			
			// DriverManager config for connection init
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/seg21-db", "root", "");
			
			if(connection != null) {
				System.out.println("Connection is Valid!");
			} else {
				System.err.println("Connection is Invalid!");
			}
			
		} catch(ClassNotFoundException cnfExc) {
			System.err.println("Missing JDBC Driver... "+cnfExc.getMessage());
		} catch(SQLException sqlExc) {
			System.err.println("Cannot Connect to Database... "+sqlExc.getMessage());
		}
		
		return connection;
		
	}
	
	public boolean insertRecord() {
		boolean isSuccess = false;
		
		try {
			Connection connection = getConnection();
			if(connection != null) {
				// if connection is valid
				PreparedStatement pStmt = connection.prepareStatement(DBOperation.INSERT_RECORD);
				
				pStmt.setInt(1, this.gear);
				pStmt.setString(2, this.brand);
				pStmt.setString(3, this.type);
				pStmt.setString(4, this.model);
				pStmt.setDouble(5, this.price);
				pStmt.setString(6, this.url);
				
				// save current record to the DB table
				pStmt.executeUpdate();
				isSuccess = true;
			} else {
				System.err.println("no.");
			}
		} catch (SQLException sqlE) {
			System.err.println(sqlE.getMessage());
		}
		
		return isSuccess;
	}
}
