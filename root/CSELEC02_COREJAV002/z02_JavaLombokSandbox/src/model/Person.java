package model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class Person {
	private @NonNull String name;
	private int age;
	private @NonNull String gender;
}
