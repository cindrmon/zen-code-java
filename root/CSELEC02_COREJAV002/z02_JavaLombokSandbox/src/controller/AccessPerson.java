package controller;

import model.*;

public class AccessPerson {

	public static void main(String[] args) {
		
		Person man = new Person();
		
		man.setName("Mohikai Mondo");
		man.setAge(26);
		man.setGender("M");
		
		System.out.printf("My name is %s. I am %d years old, and I'm %s.",
				man.getName(), man.getAge(), man.getGender()
				);

	}

}
