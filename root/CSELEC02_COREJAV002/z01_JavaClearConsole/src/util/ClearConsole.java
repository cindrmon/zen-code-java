package util;

public class ClearConsole {
	public static void clearConsole() {
		// stackoverflow solution 1:
		final String operatingSystem = System.getProperty("os.name");

		try {
			if (operatingSystem .contains("Windows")) {
			    Runtime.getRuntime().exec("cls");
			}
			else {
			    Runtime.getRuntime().exec("clear");
			}
		} catch (final Exception e) {
	         e.printStackTrace();
	    }
		
		
	}
}
