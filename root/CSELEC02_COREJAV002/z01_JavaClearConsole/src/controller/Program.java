package controller;

import util.*;

public class Program {

	public static void main(String[] args) {
		System.out.println("This is text.");
		System.out.println("This is another text.");
		System.out.println("This is starting to get cluttered up");
		System.out.println("Time to clear..");
		ClearConsole.clearConsole();
		System.out.println("Ahh, much better.");
	}

}
