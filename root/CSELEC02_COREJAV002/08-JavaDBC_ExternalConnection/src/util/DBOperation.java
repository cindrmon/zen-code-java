package util;

public interface DBOperation {

	String INSERT_RECORD = "INSERT INTO BicycleTable "
			+ "(gear, brand, type, model, price, url) "
			+ "VALUES (?,?,?,?,?,?);";
	
	String GET_ALL_RECORDS = "SELECT * FROM BicycleTable";
	
}
