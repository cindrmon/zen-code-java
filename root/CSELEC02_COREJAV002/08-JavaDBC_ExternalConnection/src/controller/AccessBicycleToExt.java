package controller;

import java.sql.ResultSet;

import model.BicycleBean;
import view.Display; 

public class AccessBicycleToExt {
	public static void main(String[] args) {
		//let us first create our bicycle object
		BicycleBean pinarelloDogma = new BicycleBean();
		pinarelloDogma.setGear(21);
		pinarelloDogma.setModel("Pinarello Dogma F12 Champagnolo");
		pinarelloDogma.setBrand("Pinarello");
		pinarelloDogma.setType("Touring");
		pinarelloDogma.setPrice(700000);
		pinarelloDogma.setUrl("https://upgrade.ph/shop/brands/pinarello" 
			+ "/pinarello-dogma-f12-disk-built-bike-campagnolo-super-record-eps/");
		
		BicycleBean cerveloP5 = new BicycleBean(); //let us create our second bike
		cerveloP5.setGear(24);
		cerveloP5.setModel("Cervelo P5");
		cerveloP5.setBrand("Cervelo");
		cerveloP5.setType("Touring");
		cerveloP5.setPrice(765000);
		cerveloP5.setUrl("https://upgrade.ph/shop/bike/cervelo/cervelo-p5-"
			+ "disc-2019-tri-tt-bike-dura-ace-9180-di2/");
		
		BicycleBean someBikeOffTheStreet = new BicycleBean();
		someBikeOffTheStreet.setGear(4);
		someBikeOffTheStreet.setModel("Generic Model");
		someBikeOffTheStreet.setBrand("Generic Brand");
		someBikeOffTheStreet.setType("Road");
		someBikeOffTheStreet.setPrice(0);
		someBikeOffTheStreet.setUrl("https://sfbay.craigslist.org/d/bicycles-by-owner/search/bik");
		
		Display.print(pinarelloDogma);	
		Display.print(cerveloP5);
		Display.print(someBikeOffTheStreet);
		
		// testing connection
//		pinarelloDogma.getConnection();
		
		// insert record to DB
//		if(pinarelloDogma.insertRecord()) {
//			System.out.println("Record Successfully Saved to DB!");
//		}
//		if (cerveloP5.insertRecord()) {
//			System.out.println("Record Successfully Saved to DB!");
//		}
//		if (someBikeOffTheStreet.insertRecord()) {
//			System.out.println("Record Successfully Saved to DB!");
//		}
		
		// call the display of current records
		ResultSet records = new BicycleBean().getAllRecords();
		
		Display.printAllRecords(records);
		
	}
}
