package demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exception {

	public static void main(String[] args) throws IOException {
		System.out.println("Enter preferred array Size: ");
		int arraySize = Integer
				.parseInt((
						new BufferedReader(
								new InputStreamReader(System.in)
								)
						).readLine()
				);
		
		int arrayNum[] = new int[arraySize];
		
		// access a valid (inbound) index
		arrayNum[arraySize-2] = 100;
		System.out.printf("[%d]: %d\n", arraySize-2, arrayNum[arraySize-2]);
		// this will run
		
		// access an invalid (outbound) index
		arrayNum[arraySize+5] = 500;
		// this will throw the error
		
	}

}
