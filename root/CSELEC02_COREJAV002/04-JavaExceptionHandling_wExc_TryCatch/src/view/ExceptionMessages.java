package view;

public interface ExceptionMessages {

	String MESSAGE_PREFIX = "Unable to process request: ";
	String GENERIC_EXCEPTION_MESSAGE = MESSAGE_PREFIX + "Please contact your system administrator for more information.";
	String NUMBER_FORMAT_EXCEPTION_MESSAGE = MESSAGE_PREFIX + "Input is not a number. Please type a number and try again.";
	String ARRAY_IOOB_EXCEPTION_MESSAGE = MESSAGE_PREFIX + "Invalid array index. Please make sure that all array indexes are inbound.";
}
