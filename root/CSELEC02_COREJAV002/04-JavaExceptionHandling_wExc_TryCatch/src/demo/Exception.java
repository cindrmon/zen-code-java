package demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import view.ExceptionMessages;

public class Exception {

	// NOTE: Using try-catch exception handling
	public static void main(String[] args) throws IOException {
		
		try {
			System.out.print("Enter preferred array Size: ");
			int arraySize = Integer
					.parseInt((
							new BufferedReader(
									new InputStreamReader(System.in)
									)
							).readLine()
					);
			
			int arrayNum[] = new int[arraySize];
			
			// access a valid (inbound) index
			arrayNum[arraySize-2] = 100;
			System.out.printf("[%d]: %d\n", arraySize-2, arrayNum[arraySize-2]);
			// this will run
			
			// access an invalid (outbound) index
//			arrayNum[arraySize+5] = 500;
			// this will throw the error
		} catch(IOException exc) {
//			exc.printStackTrace();
			System.err.println(exc.getMessage());
		} catch(NumberFormatException exc) {
			System.err.println(ExceptionMessages.NUMBER_FORMAT_EXCEPTION_MESSAGE);
//			System.err.println(exc.getMessage());
		} catch(ArrayIndexOutOfBoundsException exc) {
			System.err.println(ExceptionMessages.ARRAY_IOOB_EXCEPTION_MESSAGE);
//			System.err.println(exc.getMessage());
		} 
//		catch(Exception exc) {
//			System.out.println(ExceptionMessages.GENERIC_EXCEPTION_MESSAGE);
//		}
		finally {
			System.out.println("All Done!");
		}
		
		
		
	}

}
