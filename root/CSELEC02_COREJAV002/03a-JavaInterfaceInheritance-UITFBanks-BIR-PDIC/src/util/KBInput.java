package util;

import java.io.*;

public class KBInput {
	
	// use this instead
	private static BufferedReader getReader() {
		BufferedReader inputStream = new BufferedReader(
			new InputStreamReader(System.in));
		
		return inputStream;
		
	}
	
	public static String readString(String Message) {
		
		String input = "";
		
		System.out.print(Message);
		
		
		try {
			input = getReader().readLine();
		} catch (IOException exc) {
			System.err.println(exc.getMessage());
		}
		
		return input;
		
	}
	
public static int readInt(String Message) {
		
		int input = 0;
		
		System.out.print(Message);
		
		
		try {
			input = Integer.parseInt(getReader().readLine());
		} catch (IOException exc) {
			System.err.println(exc.getMessage());
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Input!\n" + nfe.getLocalizedMessage());
		}
		
		return input;
		
	}

public static double readDouble(String Message) {
	
	double input = 0;
	
	System.out.print(Message);
	
	
	try {
		input = Double.parseDouble(getReader().readLine());
	} catch (IOException exc) {
		System.err.println(exc.getMessage());
	} catch (NumberFormatException nfe) {
		System.err.println("Invalid Input!\n" + nfe.getLocalizedMessage());
	}
	
	return input;
	
}

}

