package controller;

import view.Display;

import util.KBInput;

import model.bank.*;
import model.regulator.*;

public class BanksController {
	
	public static void main(String[] args) {
		
		String choice;

		/**
		 * BSP is the main abstract class, and BPI, Metrobank, and SecurityBank are its
		 * children.
		 * 
		 * Class widening is done like this: 
		 * 
		 * BSP bank = new BPI(); -> will not work with multiple inheritance
		 * Display.print(bank);
		 * 
		 * BPI bank = new BPI(); -> multiple inheritance alternative
		 */
		
		/**
		 * Code snippet proving that the child object has 3 parents
		 * using instanceof
		 * 
		 * System.out.println((aBank instanceof BSP)?
		 * 		"aBank IS-A child of parent BSP";
		 * 		:"aBank IS-NOT-A child of parent BSP");
		 * 
		 * System.out.println((aBank instanceof BIR)?
		 * 		"aBank IS-A child of parent BIR";
		 * 		:"aBank IS-NOT-A child of parent BIR");
		 * 
		 * System.out.println((aBank instanceof BSP)?
		 * 		"aBank IS-A child of parent PDIC";
		 * 		:"aBank IS-NOT-A child of parent PDIC");
		 * 
		 *  */
		
		choice = KBInput.readString("Enter Bank: ");

		if(choice.equalsIgnoreCase("BPI")) {
			BPI aBank = new BPI();
			Display.print(aBank);
		}
		else if(choice.equalsIgnoreCase("Metrobank")) {
			Metrobank aBank = new Metrobank();
			Display.print(aBank);
		}
		else if(choice.equalsIgnoreCase("Security Bank")) {
			SecurityBank aBank = new SecurityBank();
			Display.print(aBank);
		}
		
	}

}
