package model.bank;

import model.regulator.*;

public class Metrobank extends BSP implements PDIC{

	private String custodianship;
	private String auditor;
//	private Object[] shares;
	
	public Metrobank() {
		System.out.println("Welcome to Metrobank US Equity Index Fund");
	}

	@Override
	public void assignCustodianship(String custodianship) {
		// TODO Auto-generated method stub
		this.custodianship = custodianship;
		System.out.printf("Custodianship assigned is %s\n", this.custodianship);

	}

	@Override
	public void assignExternalAuditor(String auditor) {
		// TODO Auto-generated method stub
		this.auditor = auditor;
		System.out.printf("External Auditor assigned is %s\n", this.auditor);
	}

	@Override
	public void buySharesFromUSCompanies() {
		// TODO Auto-generated method stub
		System.out.println("Participating in US Companies: ");
		System.out.println("Another Company\t|\t5%");
		System.out.println("Another Company 2\t|\t4%");
		System.out.println("Another Company 3\t|\t3%");
		System.out.println("Another Company 4\t|\t2%");
		System.out.println("Another Company 5\t|\t1%");
	}

	@Override
	public void investmentNotInsuredAnnouncement() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void taxAnnouncement() {
		// TODO Auto-generated method stub
		
	}

}
