package model.bank;

import model.regulator.*;

public class BPI extends BSP implements PDIC{
	
	private String custodianship;
	private String auditor;
//	private Object[] shares;
	
	public BPI() {
		System.out.println("Welcome to BPI US Equity Index Fund");
	}

	@Override
	public void assignCustodianship(String custodianship) {
		// TODO Auto-generated method stub
		this.custodianship = custodianship;
		System.out.printf("Custodianship assigned is %s\n", this.custodianship);

	}

	@Override
	public void assignExternalAuditor(String auditor) {
		// TODO Auto-generated method stub
		this.auditor = auditor;
		System.out.printf("External Auditor assigned is %s\n", this.auditor);
	}

//	@Override
//	public void buySharesFromUSCompanies(Object[] shares) {
//		// TODO Auto-generated method stub
//		System.out.println("Participating in US Companies: ");
//		for (Object share : shares) {
//			System.out.println(share.name);
//			System.out.println(share.percentShare);
//		}
//	}
	
	@Override
	public void buySharesFromUSCompanies() {
		// TODO Auto-generated method stub
		System.out.println("Participating in US Companies: ");
		System.out.println("Sample Company\t|\t5%");
		System.out.println("Sample Company 2\t|\t4%");
		System.out.println("Sample Company 3\t|\t3%");
		System.out.println("Sample Company 4\t|\t2%");
		System.out.println("Sample Company 5\t|\t1%");
	}

	@Override
	public void investmentNotInsuredAnnouncement() {
		System.out.println("All BPI UITFs are not covered by the PDIC.");
	}

	@Override
	public void taxAnnouncement() {
		System.out.printf("All BPI UITFs are subjected to %s. \n", TAX_PERCENTAGE);
		
	}

}
