package model.regulator;

public interface PDIC extends BIR{
	/**
	 * Variables declared in an interface are always
	 * `public static final`.
	 * 
	 * All fully unimplemented methods in an interface 
	 * are always `public abstract`.
	 *  */
	
	public void investmentNotInsuredAnnouncement();
}
