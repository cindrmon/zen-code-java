package view;

public interface ExceptionMessages {

	String GENERIC_EXCEPTION_MESSAGE = "Unable to process request. Please contact your system administrator for more information.";
	String NUMBER_FORMAT_EXCEPTION_MESSAGE = "Unable to process request. Input is not a number. Please type a number and try again.";
	String ARRAY_IOOB_EXCEPTION_MESSAGE = "Unable to process request. Invalid array index. Please make sure that all array indexes are inbound.";
}
