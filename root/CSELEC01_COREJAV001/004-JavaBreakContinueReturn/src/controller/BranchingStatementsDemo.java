package controller;

import java.util.Iterator;

import utility.*;

public class BranchingStatementsDemo {

	public void breakOnLoopDemo(int numberOfLoops, int breakLoop) {
		for (int i = 1; i <= numberOfLoops; i++) {
			
			System.out.printf("%d ", i);
			
				if(i == breakLoop)
					break;
				
			
		}
	}
	
	public void continueDemo(int numberOfLoops, int continueLoop) {
		for (int i = 1; i <= numberOfLoops; i++) {
									
			if(i == continueLoop)
				continue;
			
			System.out.printf("%d ", i);
	
		}
	}
	
	public static void main(String[] args) {
		
		int totalLoops = KBInput.readInt("Enter number of loops: ");
		int breakLoop = KBInput.readInt("Enter the number you want the loop to stop: ");
		int continueLoop = KBInput.readInt("Enter the number you want the loop to continue: ");
		
		new BranchingStatementsDemo().breakOnLoopDemo(totalLoops, breakLoop);
		System.out.println();
		new BranchingStatementsDemo().continueDemo(totalLoops, continueLoop);
		
	}

}
