package demonstration;

public class ProgramPolymorph {
	
	/**
	 * Program Overloading (different parameters, same method name)
	 * */

	public static void main(String[] args) {
		System.out.println("Uses String Args");
		for (String string : args) {
			System.out.println(string);
		}
		
		main();
		main(123);
		main("just a word");
		
	}
	
	public static void main() {
		System.out.println("Uses no args");
	}
	
	public static void main(String arg) {
		System.out.println("Uses one string arg");
		System.out.println(arg);
	}
	
	public static void main(int number) {
		System.out.println("Uses an int arg");
		System.out.println(number);
	}

}
