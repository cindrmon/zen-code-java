package utility;

import java.io.*;

public class KBInput {
	
	// use this instead
	private static BufferedReader getReader() {
		BufferedReader inputStream = new BufferedReader(
			new InputStreamReader(System.in));
		
		return inputStream;
		
	}
	
	public static String readString(String kbInput) {
		
		String istream = "";
		
		System.out.print(kbInput);
		
		try {
			istream = getReader().readLine();
		} catch (IOException exc) {
			System.err.println(exc.getMessage());
		}
		
		return istream;
		
	}
	
public static int readInt(String kbInput) {
		
		int istream = 0;
		
		System.out.print(kbInput);
		
		
		try {
			istream = Integer.parseInt(getReader().readLine());
		} catch (IOException exc) {
			System.err.println(exc.getMessage());
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Input!\n" + nfe.getLocalizedMessage());
		}
		
		return istream;
		
	}

public static double readDouble(String kbInput) {
	
	double istream = 0;
	
	System.out.print(kbInput);
	
	
	try {
		istream = Double.parseDouble(getReader().readLine());
	} catch (IOException exc) {
		System.err.println(exc.getMessage());
	} catch (NumberFormatException nfe) {
		System.err.println("Invalid Input!\n" + nfe.getLocalizedMessage());
	}
	
	return istream;
	
}

}
