package controller;

import utility.*;

public class StringComparisonDemo {

	// this is a centralized/static method
	public static void ifElseCaseSensitiveDemo(String FaveDay) {
		
			if(FaveDay.equals("Sunday")) {
				System.out.println("Fridays are better than sundays, cuz sundays are my suicide days.");
			} else if (FaveDay.equals("Monday")) {
				System.out.println("Just another manic monday");
			} else if (FaveDay.equals("Tuesday")) {
				System.out.println("Ugh");
			} else if (FaveDay.equals("Wednesday")) {
				System.out.println("Live chat this wednesday! See description for more details");
			} else if (FaveDay.equals("Thursday")) {
				System.err.println("Finally, I can rest");
			} else if (FaveDay.equals("Friday")) {
				System.out.println("TGIF!");
			} else if (FaveDay.equals("Saturday")) {
				System.out.println("Nah fam, I hate weekends.");
			}
		
	}
	
	public static void ifElseCaseInsensitiveDemo(String FaveDay) {
		
		if(FaveDay.equalsIgnoreCase("Sunday")) {
			System.out.println("Fridays are better than sundays, cuz sundays are my suicide days.");
		} else if (FaveDay.equalsIgnoreCase("Monday")) {
			System.out.println("Just another manic monday");
		} else if (FaveDay.equalsIgnoreCase("Tuesday")) {
			System.out.println("Ugh");
		} else if (FaveDay.equalsIgnoreCase("Wednesday")) {
			System.out.println("Live chat this wednesday! See description for more details");
		} else if (FaveDay.equalsIgnoreCase("Thursday")) {
			System.err.println("Finally, I can rest");
		} else if (FaveDay.equalsIgnoreCase("Friday")) {
			System.out.println("TGIF!");
		} else if (FaveDay.equalsIgnoreCase("Saturday")) {
			System.out.println("Nah fam, I hate weekends.");
		}
	
}

	// this is a federated method
	
	public void switchDemo(String FaveDay) {
		
		switch (FaveDay.toLowerCase()) {
		
			case "sunday":
				System.out.println("Fridays are better than sundays, cuz sundays are my suicide days.");
				break;
			
			case "monday":
				System.out.println("Just another manic monday");
				break;
				
			case "tuesday":
				System.out.println("Ugh");
				break;
				
			case "wednesday":
				System.out.println("Live chat this wednesday! See description for more details");
				break;
				
			case "thursday":
				System.err.println("Finally, I can rest");
				break;
				
			case "friday":
				System.out.println("TGIF!");
				break;
				
			case "saturday":
				System.out.println("Nah fam, I hate weekends.");
				break;
			
			default:
				System.out.println("Invalid Day, bucko");
		}
		
	}
	
	public static void main(String[] args) {
		
		String day = KBInput.readString("Enter your favorite day of the week: ");

		// using a static method in the same class
		// can be called directly
		// ifElseCaseSensitiveDemo(day);
		// ifElseCaseInsensitiveDemo(day);
		
		// using a non-static method
		// must create an object first
		StringComparisonDemo object = new StringComparisonDemo();
		object.switchDemo(day);
		

	}

}
