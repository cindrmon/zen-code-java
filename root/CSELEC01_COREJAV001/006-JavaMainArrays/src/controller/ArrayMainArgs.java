package controller;

public class ArrayMainArgs {

	public static void main(String[] args) {
		
		
		if (args.length > 0) {
			System.out.println("There are arguments to the main() method");
			System.out.println("Number of args: " + args.length);
			for (String string : args) {
				System.out.println(string);
			}
		}
		else {
			System.out.println("There are no arguments to the main() method");
		}

	}

}
