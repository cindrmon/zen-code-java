package main;

public class BasicsOfJava {
	
	static void methodThingy() {
		System.out.println("method");
	}
	
	static void anotherMethodThingy(int num1, int num2) {
		int a_local_variable = 3; // automatic variable
		System.out.println(num1 + num2 + a_local_variable);
	}

	static int redundantFunction(String word) {
		return word.length();
	}
	
	static void shittyThings(boolean isShit) {
		if (isShit) {
			System.out.println("is the shit");
		}
		else {
			System.out.println("is not the shit");
		}
	}
	
	public static void main(String[] args) {
		
		//	Primitive Data types
		long variable = 2147483648L;
		float a, b;
		a = 0.1f;
		b = 0.2f;
		double c, d;
		c = 10.10013232;
		d = 9.8231274141924;
		boolean isThisShit = false;
		// boolean isThisTooLongEnoughForItToBecomeAUsableVariableInThisJavaProgramThatIMadeOnTheSpotBecauseItIsARequirementForGraduationAndThatIMightUseThisForWorkAndStuff = true;
		// no u
		
		int operandOne = 5;
		int operandTwo = 3;
		
		//	Class-based Data Types
		String thisIsAString, theresMore;
		
		// all sysouts		
		System.out.println(5 + 2);
		methodThingy();
		anotherMethodThingy(5, 2);
		System.out.println(redundantFunction("owo what's this?"));
		System.out.printf("a long variable is %d\n", variable);
		
		System.out.println(a + b);
		System.out.println(c - d);
		System.out.println((a + b) * (c - d));
		
		shittyThings(isThisShit);
		isThisShit = true;
		shittyThings(isThisShit);
		
		thisIsAString = "Dear God";
		theresMore = "no";
		
		System.out.println(thisIsAString + theresMore);
		
		int productOfOperands = operandOne & operandTwo;
		int sumOfOperands = operandOne | operandTwo;
		
		System.out.printf("\n\nSum of operands %d and %d using '|': %d\n", operandOne, operandTwo, sumOfOperands);
		System.out.printf("Product of operands %d and %d using '&': %d\n\n", operandOne, operandTwo, productOfOperands);
	
		// ?: in use (skips if statements)
		
		boolean sample = false;
		
		String answer = sample? "true" : "false";
		System.out.println(answer);
		
		sample = true;
		answer = sample? "true" : "false";
		System.out.println(answer);
		
	}

}
