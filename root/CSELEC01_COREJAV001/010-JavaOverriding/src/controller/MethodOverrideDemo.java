package controller;

import model.*;

public class MethodOverrideDemo {

	public static void main(String[] args) {
		
		new Parent().printMessage();
		new Child().printMessage();

	}

}
