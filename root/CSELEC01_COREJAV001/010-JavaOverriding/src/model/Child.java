package model;

public class Child extends Parent {
	
	/**
	 * Program Overriding (same parameters, same method name, different method)
	 * */
	
	@Override
	public void printMessage() {
		System.out.println("I am a child");
	}
}
