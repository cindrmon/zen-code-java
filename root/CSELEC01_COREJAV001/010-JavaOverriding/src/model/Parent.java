package model;

public class Parent {
	
	/**
	 * Program Overriding (same parameters, same method name, different method)
	 * */
	
	public void printMessage() {
		System.out.println("I am a parent");
	}
}
