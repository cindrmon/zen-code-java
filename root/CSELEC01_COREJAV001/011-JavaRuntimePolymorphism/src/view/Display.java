package view;

import model.*;

public class Display {
	public static void print(Hero hero) {
		
		// instanceof keyword to perform introspection
		if(hero instanceof ProfessorX) {
			ProfessorX xavier = (ProfessorX) hero;
			System.out.println(xavier.name);
			xavier.ability();
		}
		else if(hero instanceof Archangel) {
			Archangel woodsworth = (Archangel) hero;
			System.out.println(woodsworth.name);
			woodsworth.ability();
		} //...
		else if(hero instanceof Hero) {
			System.out.println(hero.name);
			hero.ability();
		}
		else {
			System.err.println("This is not a member of 'hero' class.");
		}
		
	}
}
