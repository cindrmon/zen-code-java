package model;

public class DistractedMan extends Hero {

	@Override
	public void ability() {
		name = "Distracted Man";
		System.out.println(name + " needs to buy an ice cream brb");
	}
	
	@Override
	public void stuff() {
		System.out.println("These are the stuffs of Distracted Man!");
	}
	
}
