package controller;

import model.*;
import view.*;

public class AccessHro {

	public static void main(String[] args) {
		
		
		// sample static binding (non-polymorphism)
		// Wolvering Logan = new Wolverine();
		// Parent name = new Child(); --> Object Polymorphism
		Hero Logan = new Wolverine();
		Logan.name = "Wolverine";
		Logan.ability(); // dynamic binding
		
		// runtime polymorphism
		Hero hero = null; // dynamic binding (implicit casting)/ upcasting
        // a comment to test permissions
		
		hero = new Cyclops();
		hero.name = "Scott Summers";
		System.out.println(hero.name);
		hero.ability();
		
		hero = new Beast();
		hero.name = "The Beast";
		System.out.println(hero.name);
		
		// introspection (explicit casting)
		System.out.println("This one is from view.Display");
		Display.print(hero);
		
	}

}
