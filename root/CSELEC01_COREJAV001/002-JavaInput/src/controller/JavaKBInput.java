package controller;

import utility.KBInput;

public class JavaKBInput {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name = KBInput.readString("Enter Name: ");
		String course = KBInput.readString("Enter Course: ");
		int yearLevel = KBInput.readInt("Enter Year Level: ");
		double targetGPA = KBInput.readDouble("Enter target GPA: ");
		
		System.out.printf("Your name is %s, and your course is %s.\nYou are currently in Year %d, and your target GPA is %f. ", name, course, yearLevel, targetGPA);

	}

}
